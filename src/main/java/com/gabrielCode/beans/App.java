package com.gabrielCode.beans;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

public class App {
	public static void main(String[] args) {
		ApplicationContext appContext = new 
				ClassPathXmlApplicationContext("com/gabrielCode/xml/beans.xml");
		Mundo m = (Mundo)appContext.getBean("mundo");
		System.out.println(m.getSaludo());
		((ConfigurableApplicationContext)appContext).close();
	}
}
